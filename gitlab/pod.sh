pods_available() {
    STATUS=$(kubectl get pods -l app=$1 -n $2 -o jsonpath="{.items[*].status.containerStatuses[*].ready}")
    string="${STATUS}"
    echo $string
    substring='false'
    if test "${string#*$substring}" != "$string"
    then
        echo "PODS are not READY yet"    # $substring is in $string
    else
        echo "PODS are READY now"
        exit 0    # $substring is not in $string
    fi
    if test $3 -eq 30;
    then
        echo "EXIT"
        exit 1
    fi
}


max=30
for i in `seq 2 $max`
do
	pods_available $1 $2 $i
    sleep 30
done