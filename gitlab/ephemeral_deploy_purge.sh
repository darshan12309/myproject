#!/bin/sh

deployments=$(kubectl get deploy -n $2 | grep $1 | awk '{ print $1; }')
echo $deployments

for i in $deployments;
do
	echo $i
	helm uninstall $i -n $2
done
