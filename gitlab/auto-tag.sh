#!/bin/bash

# Get the current commit hash
CURRENT_COMMIT=$(git rev-parse HEAD)
echo "CURRENT_COMMIT: $CURRENT_COMMIT";

git config --global user.name "$GITLAB_USER";
git config --global user.email "$GITLAB_USER";
git remote set-url origin https://$GITLAB_USER:$GITLAB_ACCESS_TOKEN@gitlab.com/${CI_PROJECT_PATH}.git;
git fetch --tags
tags=$(git tag --sort=-v:refname)

echo $tags
# Extract the latest semver tag
last_semver_tag=$(echo "$tags" | grep -E '^[0-9]+\.[0-9]+\.[0-9]+$' | head -n1)

echo $last_semver_tag

# If no semver tag found, exit
if [ -z "$last_semver_tag" ]; then
    echo "No semver tags found."
    exit 1
fi

# Extract major, minor, and patch versions from the last tag
IFS='.' read -r -a version_parts <<< "$last_semver_tag"
major=${version_parts[0]}
minor=${version_parts[1]}
patch=${version_parts[2]}

# Increment the version based on the type (major, minor, patch)
case "$1" in
    major)
        major=$((major + 1))
        minor=0
        patch=0
        ;;
    minor)
        minor=$((minor + 1))
        patch=0
        ;;
    patch)
        patch=$((patch + 1))
        ;;
    *)
        echo "Usage: $0 {major|minor|patch}"
        exit 1
        ;;
esac

# Create the new tag
RELEASE_TAG_NAME="$major.$minor.$patch"
echo "Adding Tag: $RELEASE_TAG_NAME";
echo "GITLAB_USER: $GITLAB_USER";
echo "CURRENT_COMMIT: $CURRENT_COMMIT"
git config --global user.name "$GITLAB_USER";
git config --global user.email "$GITLAB_USER";
git remote set-url origin https://$GITLAB_USER:$GITLAB_ACCESS_TOKEN@gitlab.com/${CI_PROJECT_PATH}.git;
git branch
ls -lrt
echo "CI_COMMIT_BRANCH: $CI_COMMIT_BRANCH"
echo "CI_MERGE_REQUEST_SOURCE_BRANCH_NAME: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
BRANCH_NAME=${CI_COMMIT_BRANCH:-$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}

if [ -z "$BRANCH_NAME" ]; then
    echo "Branch name could not be determined. Exiting."
    exit 1
fi

echo "Branch Name: $BRANCH_NAME"
git checkout $BRANCH_NAME
ls -lrt
git branch
git tag $RELEASE_TAG_NAME
git tag

# Push the tag and check for failure
if ! git push origin $RELEASE_TAG_NAME; then
    echo "Failed to push the tag $RELEASE_TAG_NAME"
    exit 1
fi

echo "IMAGE_TAG: $IMAGE_TAG"
echo "BUILD_IMAGE: $BUILD_IMAGE"
echo "DOCKER_REGISTRY: $DOCKER_REGISTRY"

export ECR_REPO=$APP_NAME-stage
echo "ECR_REPO: $ECR_REPO"

export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
export AWS_ACCESS_KEY_ID=$DOCKER_AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$DOCKER_AWS_SECRET_ACCESS_KEY
aws ecr get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY
aws ecr describe-repositories --repository-names $ECR_REPO || false || if [ $? -ne 0 ]; then echo "Repo did not exists check $TF_REPO job $ENV_NAME branch"; exit 1; fi;
echo "docker pull"
docker pull $IMAGE_TAG

docker tag $IMAGE_TAG "$DOCKER_REGISTRY/$ECR_REPO:$RELEASE_TAG_NAME"
echo "docker images"
docker images
echo "release:Tag $DOCKER_REGISTRY/$ECR_REPO:$RELEASE_TAG_NAME"
docker push "$DOCKER_REGISTRY/$ECR_REPO:$RELEASE_TAG_NAME"
echo "docker $RELEASE_TAG_NAME pushed"